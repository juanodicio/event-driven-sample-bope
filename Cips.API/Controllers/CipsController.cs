using Cips.Application.Cips.Commands;
using Cips.Application.Common.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Cips.API.Models;
using Cips.Domain.Entities;


namespace Cips.API.Controllers
{
    [ApiController]
    [Route("v1/cips")]
    public class CipsController : ControllerBase
    {
        private IEventBus _eventBus;

        public CipsController(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        [HttpPost]
        public ActionResult<Cip> CreateCip(CipForCreationDto cipData)
        {
            var cmdRequest = new CreateCipRequest()
            {
                CurrencyCode = cipData.CurrencyCode,
                Total = cipData.Total,
                UserEmail = cipData.UserEmail
            };
            
            // Otro comentario
            
            var createCipCommand = new CreateCipCommand(_eventBus);
            var cip = createCipCommand.Handle(cmdRequest);
            return cip;
        }
        
        
        [HttpPost]
        [Route("v2")]
        public ActionResult<Cip> CreateCipV2(CipForCreationDto cipData)
        {
            var cmdRequest = new CreateCipRequest()
            {
                CurrencyCode = cipData.CurrencyCode,
                Total = cipData.Total,
                UserEmail = cipData.UserEmail
            };
            
            var createCipCommand = new CreateCipCommand(_eventBus);
            return createCipCommand.Handle(cmdRequest);
        }

    }
}